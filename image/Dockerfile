ARG FUNCTION_DIR="/handler"

# Build lambda handler wrapper
FROM python:3.8.9-buster as build-image

# Include global arg in this stage of the build
ARG FUNCTION_DIR

# Install aws-lambda-cpp build dependencies
RUN apt-get update && \
  apt-get install -y \
  g++ \
  make \
  cmake \
  unzip \
  libcurl4-openssl-dev

# Create function directory
RUN mkdir -p ${FUNCTION_DIR}

# Copy handler function
COPY app/* ${FUNCTION_DIR} 

# Install the function's dependencies
RUN pip install \
  --target ${FUNCTION_DIR} \
  awslambdaric

FROM thomasweise/docker-texlive-full:1.1

# Include global arg in this stage of the build
ARG FUNCTION_DIR

# TexLive runs on Ubuntu and has old apt-get repos
# Update apt-get repos from "archive.|security." -> "old-releases"
RUN sed -i -e 's/archive.ubuntu.com\|security.ubuntu.com/old-releases.ubuntu.com/g' /etc/apt/sources.list

# Install pip and locales
RUN apt-get update -y  && \
  apt-get install -y \
  python3-pip locales

RUN pip3 install boto3 

# Provide a modified copy of pythontex3 that can run in AWS Lambda (aws prevents pools in Lambda)
COPY /pythontex3.py /usr/share/texlive/texmf-dist/scripts/pythontex

# Hard alias python -> python3
RUN cp /bin/python3 /bin/python

# Set locale to en_US 
RUN locale-gen en_US.UTF-8
ENV LC_ALL=en_US.UTF-8

# Set working directory to function root directory
WORKDIR ${FUNCTION_DIR}

# Copy in the built dependencies
COPY --from=build-image ${FUNCTION_DIR} ${FUNCTION_DIR}

ENTRYPOINT [ "/bin/python3", "-m", "awslambdaric" ]
CMD [ "lambda.handler" ]