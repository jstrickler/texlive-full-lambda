import os
import io
import json
import shutil
import subprocess
import base64
import zipfile
import boto3

# accepts an SQS event
def handler(event, context):

    print(event)

    s3_event = json.loads(event['Records'][0]['body'])
    s3_object = s3_event['Records'][0]['s3']
    input_bucket = s3_object['bucket']['name']
    input_key = s3_object['object']['key']
    output_bucket = os.environ['OutputBucket']
    output_key = os.path.splitext(input_key)[0] + '.pdf'

    # Extract input ZIP file to /tmp/latex...
    shutil.rmtree("/tmp/latex", ignore_errors=True)
    os.mkdir("/tmp/latex")

    input = boto3.client('s3').get_object(Bucket=input_bucket,
                                      Key=input_key)
    bytes = input["Body"].read()

    z = zipfile.ZipFile(io.BytesIO(bytes))
    z.extractall(path="/tmp/latex")

    for file in os.listdir("/tmp/latex"):
        if file.endswith(".tex"):
            basename = os.path.basename(file).split(".")[0]
            break

    os.environ['HOME'] = "/tmp/latex/"

    os.chdir("/tmp/latex/")

    # Compile latex process for the first time to generate pythontex needed files
    first_compile = subprocess.run(['pdflatex', basename+'.tex'],
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)

    # Run pythontex process to fill data fields from json
    python_proc = subprocess.run(['pythontex', basename+'.tex', '--rerun=always', '--verbose'],
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.STDOUT)

    print(python_proc.stdout.decode('utf-8'))

    # Check to ensure pythontex files were generated successfully, raises CalledProcess error if so
    python_proc.check_returncode()

    # Compile process to pdf
    r = subprocess.run(['pdflatex', basename+'.tex'],
                       stdout=subprocess.PIPE,
                       stderr=subprocess.STDOUT)


    # Copy metadata fields to the new PDF object
    metadata = {}

    if 'version' in input['Metadata']:
      metadata['version'] = input['Metadata']['version']

    if 'identifier' in input['Metadata']:
      metadata['identifier'] = input['Metadata']['identifier']

    boto3.client('s3').upload_file(basename + ".pdf",
                                   output_bucket,
                                   output_key,
                                   ExtraArgs={
                                     'ContentType': 'application/pdf',
                                     'Metadata': metadata
                                   })

    return {
        'first_out': first_compile.stdout.decode('utf-8'),
        'python_proc': python_proc.stdout.decode('utf-8'),
        'stdout': r.stdout.decode('utf_8')
    }
